﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClothBazar1.Web.Startup))]
namespace ClothBazar1.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
